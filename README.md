# This is early alpha version of server
## Use at your own risk

[![pipeline status](https://gitlab.com/alexrett/moneykeeper_proto_server/badges/master/pipeline.svg)](https://gitlab.com/alexrett/moneykeeper_proto_server/commits/master)

### How to install 

0. You can download latest build version from [releases](https://gitlab.com/alexrett/moneykeeper_proto_server/releases)

1. clone repository to your server

2. install dependencies

    ```
    go get github.com/araddon/dateparse
    go get github.com/gin-gonic/gin
    go get github.com/jinzhu/gorm
    go get github.com/satori/go.uuid
    go get golang.org/x/crypto/bcrypt
    go get github.com/jinzhu/gorm/dialects/sqlite
    ```

3. `mkdir -p /opt/mk/`
4. build main.go `go build -o /opt/mk/moneykeeper main.go`
5. `chmod +x /opt/mk/moneykeeper`
6. `cp moneykeeper.service /etc/systemd/system/`
7. setup new service
    ```
    systemctl daemon-reload
    systemctl enable moneykeeper.service
    systemctl start moneykeeper.service
    ```

8. then setup your web server

```
cat nginx.conf > /etc/nginx/sites-available/moneykeeper
nano /etc/nginx/sites-available/moneykeeper
ln -s /etc/nginx/sites-available/moneykeeper /etc/nginx/sites-enabled/
nginx -t

```
use certbot if needed

