package main

import (
	"fmt"
	"github.com/araddon/dateparse"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strings"
	"time"

	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

type Logs struct {
	SyncDate string
	Logs     []Log
}

type Time struct {
	time.Time
}

func (t *Time) String() string {
	return t.Time.String()
}

func (t *Time) UnmarshalJSON(b []byte) error {
	s := strings.Trim(string(b), "\"")
	if s == "null" {
		t.Time = time.Time{}
		return nil
	}
	var err error
	t.Time, err = dateparse.ParseAny(s)
	return err
}

type Log struct {
	Id     uuid.UUID `json:"id" gorm:"type:varchar(36);primary_index"`
	Action string    `json:"action" gorm:"type:varchar(10)"`
	Date   Time      `json:"date" gorm:"type:datetime"`
	User   uuid.UUID `json:"user" gorm:"type:varchar(36)"`
	Table  string    `json:"table" gorm:"type:varchar(20)"`
	Data   string    `json:"data" gorm:"type:text"`
}

type User struct {
	Id       uuid.UUID `json:"id" gorm:"type:varchar(36)"`
	Name     string    `json:"name" gorm:"type:varchar(50)"`
	Login    string    `json:"login" gorm:"type:varchar(50);primary_index;"`
	Password string    `json:"password" gorm:"type:varchar(255)"`
}

type Device struct {
	Id       uuid.UUID `json:"id" gorm:"type:varchar(36);primary_index"`
	ApiKey   uuid.UUID `json:"api_key" gorm:"type:varchar(36)"`
	User     uuid.UUID `json:"user" gorm:"type:varchar(36)"`
	IsActive bool
}

type Sync struct {
	Log    uuid.UUID `json:"id" gorm:"type:varchar(36);primary_index"`
	Device uuid.UUID `json:"device" gorm:"type:varchar(36);primary_index"`
	Status bool
}

var db *gorm.DB
var apiKeyString string

func checkAdminHeader(context *gin.Context) error {
	type SaveLogResponseV1 struct {
		Error   bool
		Message string
	}
	var response SaveLogResponseV1
	apiKey := context.GetHeader("ApiKey")
	if apiKey != apiKeyString {
		response.Error = true
		response.Message = "No key"
		context.JSON(http.StatusBadRequest, response)

		return fmt.Errorf("%s", "bad request")
	}

	return nil
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func RandStringBytes(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

func main() {
	var err error
	apiKeyString = os.Getenv("MK_ADMIN_API_KEY")
	if apiKeyString == "" {
		log.Println("env MK_ADMIN_API_KEY is empty")
		apiKeyString = RandStringBytes(32)
		log.Println("generate random api key")
		log.Println(apiKeyString)

	}
	dbpath := os.Getenv("MK_DB")
	if dbpath == "" {
		log.Println("env MK_DB is empty, use default - moneykeeper.db")
		dbpath = "moneykeeper.db"
	}
	var addr = os.Getenv("MK_ADDR")
	if addr == "" {
		log.Fatal("env MK_ADDR is empty, use default - localhost:8085")
		addr = "localhost:8085"
	}
	db, err = gorm.Open("sqlite3", dbpath)
	if err != nil {
		log.Fatal(err)
	}

	db.LogMode(true)
	db.AutoMigrate(&User{}, &Device{}, &Log{}, &Sync{})

	srv := gin.Default()

	srv.GET("/", func(context *gin.Context) {
		context.Writer.WriteHeader(http.StatusOK)
		context.Writer.Write([]byte(renderHomepage()))
	})

	srv.GET("/policy", func(context *gin.Context) {
		context.Writer.WriteHeader(http.StatusOK)
		context.Writer.Write([]byte(renderPolicy()))
	})

	srv.GET("/registration", func(context *gin.Context) {
		context.Writer.WriteHeader(http.StatusOK)
		context.Writer.Write([]byte(renderRegistration()))
	})

	srv.GET("/api/v1/devices", func(context *gin.Context) {
		if err != checkAdminHeader(context) {
			return
		}
		var devices []Device
		db.Find(&devices)
		context.JSON(200, devices)
	})

	srv.GET("/api/v1/users", func(context *gin.Context) {
		if err != checkAdminHeader(context) {
			return
		}
		var users []User
		db.Find(&users)
		context.JSON(200, users)
	})

	srv.GET("/api/v1/clearAll", func(context *gin.Context) {
		if err != checkAdminHeader(context) {
			return
		}
		db.Exec("delete from syncs")
		db.Exec("delete from users")
		db.Exec("delete from devices")
		db.Exec("delete from logs")
		context.JSON(200, "OK")
	})

	srv.GET("/api/v1/clearSyncs", func(context *gin.Context) {
		if err != checkAdminHeader(context) {
			return
		}
		db.Exec("delete from  syncs")
		db.Exec("delete from  logs")
		context.JSON(200, "OK")
	})

	srv.GET("/api/v1/clearSyncsForDevice", func(context *gin.Context) {
		if err != checkAdminHeader(context) {
			return
		}
		db.Exec(fmt.Sprintf("update syncs set status = false where device = '%s'", context.Request.URL.Query().Get("device")))
		context.JSON(200, "OK")
	})

	srv.GET("/api/v1/restoreAll", func(context *gin.Context) {
		if err != checkAdminHeader(context) {
			return
		}
		db.Exec("UPDATE syncs SET status = false")
		context.JSON(200, db.Exec("UPDATE syncs SET status = false").RowsAffected)
	})

	srv.POST("/api/v1/createUser", func(context *gin.Context) {
		if err != checkAdminHeader(context) {
			return
		}
		var user User
		var err error

		type Req struct {
			Login    string
			Password string
			Name     string
		}

		var req Req

		err = context.BindJSON(&req)

		if err != nil {
			context.JSON(500, err.Error())
		}

		user.Id = uuid.NewV4()
		user.Password = hashAndSalt([]byte(req.Password))
		user.Login = req.Login
		user.Name = req.Name

		if err != nil {
			context.JSON(500, err.Error())
		}
		log.Println(user)
		db.Create(&user)

		context.JSON(200, "OK")

	})

	srv.POST("/api/v1/updateUser", func(context *gin.Context) {
		if err != checkAdminHeader(context) {
			return
		}
		var err error
		type LoginResponseV1 struct {
			Error   bool
			Message string
		}
		var response LoginResponseV1
		type Req struct {
			Login    string
			Password string
		}

		var req Req

		err = context.BindJSON(&req)

		if err != nil {
			context.JSON(500, err.Error())
		}

		var user User
		db.Where("login = ?", req.Login).First(&user)

		if user.Login == "" {
			response.Error = true
			response.Message = err.Error()
			context.JSON(http.StatusUnauthorized, response)

			return
		}

		user.Password = hashAndSalt([]byte(req.Password))
		db.Table("users").Where("id = ?", user.Id).Update(&user)

		context.JSON(200, "OK")

	})

	srv.POST("/api/v1/login", func(context *gin.Context) {
		type LoginPostV1 struct {
			Login    string
			Password string
			Device   string
		}

		type LoginResponseV1 struct {
			UserId  uuid.UUID
			ApiKey  uuid.UUID
			Error   bool
			Message string
		}

		var req LoginPostV1
		var response LoginResponseV1

		err := context.BindJSON(&req)
		if err != nil {
			response.Error = true
			response.Message = err.Error()
			context.JSON(http.StatusUnauthorized, response)

			return
		}

		var user User
		db.Where("login = ?", req.Login).First(&user)

		if user.Login == "" {
			response.Error = true
			response.Message = err.Error()
			context.JSON(http.StatusUnauthorized, response)

			return
		}

		log.Println(user.Password, req.Password, hashAndSalt([]byte(req.Password)), comparePasswords(user.Password, []byte(req.Password)))
		if !comparePasswords(user.Password, []byte(req.Password)) {
			response.Error = true
			response.Message = "not equal"
			context.JSON(http.StatusUnauthorized, response)

			return
		}

		var device Device

		db.Where("user = ? and id = ?", user.Id, strings.ToLower(req.Device)).First(&device)

		if strings.ToLower(device.Id.String()) != strings.ToLower(req.Device) {
			newDevice := Device{}
			newDevice.Id, err = uuid.FromString(req.Device)
			newDevice.User = user.Id
			newDevice.IsActive = true
			newDevice.ApiKey = uuid.NewV4()

			if err == nil {
				db.Create(&newDevice)
				response.ApiKey = newDevice.ApiKey
				response.UserId = user.Id
				response.Error = false
				response.Message = ""
			} else {
				response.Error = true
				response.Message = err.Error()
				context.JSON(http.StatusUnauthorized, response)

				return

			}
		} else {
			response.ApiKey = device.ApiKey
			response.UserId = user.Id
			response.Error = false
			response.Message = ""
		}

		context.JSON(200, response)
	})

	srv.POST("/api/v1/saveLog", func(context *gin.Context) {
		type SaveLogResponseV1 struct {
			Error   bool
			Message string
		}

		var response SaveLogResponseV1

		apiKey := context.GetHeader("ApiKey")
		if apiKey == "" {
			response.Error = true
			response.Message = "No key"
			context.JSON(http.StatusBadRequest, response)

			return
		}

		var device Device

		db.Where("api_key = ?", strings.ToLower(apiKey)).First(&device)

		if !device.IsActive {
			response.Error = true
			response.Message = "Inactive"
			context.JSON(http.StatusBadRequest, response)

			return
		}

		var requestLogs Logs
		err := context.BindJSON(&requestLogs)
		if err != nil {
			response.Error = true
			response.Message = "Bad json"
			context.JSON(http.StatusBadRequest, response)

			return
		}

		go saveLogs(requestLogs, apiKey)
		//log.Println(device.Id.String(), uuid.Nil, (device.Id == uuid.Nil))

		context.JSON(200, response)
	})

	srv.POST("/api/v1/saveLogAll", func(context *gin.Context) {
		type SaveLogResponseV1 struct {
			Error   bool
			Message string
		}

		var response SaveLogResponseV1

		apiKey := context.GetHeader("ApiKey")
		if apiKey == "" {
			response.Error = true
			response.Message = "No key"
			context.JSON(http.StatusBadRequest, response)

			return
		}

		var device Device

		db.Where("api_key = ?", strings.ToLower(apiKey)).First(&device)

		if !device.IsActive {
			response.Error = true
			response.Message = "Inactive"
			context.JSON(http.StatusBadRequest, response)

			return
		}

		var requestLogs Logs
		err := context.BindJSON(&requestLogs)
		if err != nil {
			response.Error = true
			response.Message = "Bad json"
			context.JSON(http.StatusBadRequest, response)

			return
		}

		go saveLogsAll(requestLogs, apiKey)
		//log.Println(device.Id.String(), uuid.Nil, (device.Id == uuid.Nil))

		context.JSON(200, response)
	})

	srv.GET("/api/v1/sync", func(context *gin.Context) {
		type SyncResponseV1 struct {
			Error   bool
			Message string
			Logs    []Log
		}

		response := SyncResponseV1{Logs: []Log{}}

		apiKey := context.GetHeader("ApiKey")
		if apiKey == "" {
			response.Error = true
			response.Message = "No key"
			context.JSON(http.StatusBadRequest, response)

			return
		}

		var device Device

		db.Where("api_key = ?", strings.ToLower(apiKey)).First(&device)

		if !device.IsActive {
			response.Error = true
			response.Message = "Inactive"
			context.JSON(http.StatusBadRequest, response)

			return
		}

		var syncs []Sync

		db.Where("device = ? and status = ?", device.Id.String(), false).Find(&syncs)
		//response.Data = []log{}
		var ids []string

		for _, v := range syncs {
			var l Log
			db.Where("id = ?", v.Log.String()).First(&l)
			if l.Id != uuid.Nil {
				response.Logs = append(response.Logs, l)
				ids = append(ids, v.Log.String())

			}
		}
		go db.Table("syncs").Where("log IN (?) and device = ? and status = 0", ids, strings.ToLower(device.Id.String())).Updates(map[string]interface{}{"status": true})

		context.JSON(200, response)
	})

	log.Fatal(srv.Run(addr))
}

func saveLogs(logs Logs, apiKey string) {
	var devices []Device
	db.Where("api_key != ?", strings.ToLower(apiKey)).Find(&devices)
	for _, v := range logs.Logs {
		db.Create(&v)
		for _, d := range devices {
			s := Sync{}
			s.Device = d.Id
			s.Log = v.Id
			s.Status = false
			db.Create(&s)
		}
	}
}

func saveLogsAll(logs Logs, apiKey string) {
	var devices []Device
	db.Where("api_key != ?", strings.ToLower(apiKey)).Find(&devices)
	for _, v := range logs.Logs {
		db.Create(&v)
		for _, d := range devices {
			s := Sync{}
			s.Device = d.Id
			s.Log = v.Id
			s.Status = false
			db.Create(&s)
		}
	}
}

func hashAndSalt(pwd []byte) string {
	hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.MinCost)
	if err != nil {
		log.Println(err)
	}
	return string(hash)
}

func comparePasswords(hashedPwd string, plainPwd []byte) bool {
	byteHash := []byte(hashedPwd)
	err := bcrypt.CompareHashAndPassword(byteHash, plainPwd)
	if err != nil {
		log.Println(err)
		return false
	}

	return true
}

func renderHomepage() string {
	return `
<!DOCTYPE html>
    <html>
    <head>
      <meta charset='utf-8'>
      <meta name='viewport' content='width=device-width'>
      <title>Sloth Money Keeper</title>
      <style> body { font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; padding:1em; } </style>
    </head>
    <body>
<h1>Sloth Money Keeper</h1>
<p>This is a demo server, only for author personal usage, for your own server you can go to <a href="https://gitlab.com/alexrett/moneykeeper_proto_server">git repository</a> and build it for your own.</p>
<p>I created this server just for learning and for some personal reasons.</p>
<p>If you want to use my app, just deploy your own server and change host while login in application.</p>
<p>Have a nice day.</p>
	</body>
	</html>
`
}

func renderRegistration() string {
	return `
<!DOCTYPE html>
    <html>
    <head>
      <meta charset='utf-8'>
      <meta name='viewport' content='width=device-width'>
      <title>Registration</title>
      <style> body { font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; padding:1em; } </style>
    </head>
    <body>
<h1>Sloth Money Keeper</h1>
<p>Registration temporary unavailable</p>
	</body>
	</html>
`
}

func renderPolicy() string {
	return `

<!DOCTYPE html>
    <html>
    <head>
      <meta charset='utf-8'>
      <meta name='viewport' content='width=device-width'>
      <title>Privacy Policy</title>
      <style> body { font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; padding:1em; } </style>
    </head>
    <body>
    <h2>Privacy Policy</h2> <p> Malikov A.V. built the Sloth Money Keeper app as a Free app. This SERVICE is provided by
                    Malikov A.V. at no cost and is intended for use as is.
                  </p> <p>This page is used to inform visitors regarding my policies with the collection, use, and disclosure
                    of Personal Information if anyone decided to use my Service.
                  </p> <p>If you choose to use my Service, then you agree to the collection and use of information in
                    relation to this policy. The Personal Information that I collect is used for providing and improving
                    the Service. I will not use or share your information with anyone except as described
                    in this Privacy Policy.
                  </p> <p>The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is
                    accessible at Sloth Money Keeper unless otherwise defined in this Privacy Policy.
                  </p> <p><strong>Information Collection and Use</strong></p> <p>For a better experience, while using our Service, I may require you to provide us with certain
                    personally identifiable information. The information that I request will be retained on your device and is not collected by me in any way.
                  </p> <p>The app does use third party services that may collect information used to identify you.</p> <div><p>Link to privacy policy of third party service providers used by the app</p> <ul><li><a href="https://www.google.com/policies/privacy/" target="_blank">Google Play Services</a></li><!----><!----><!----><!----><!----><!----><!----></ul></div> <p><strong>Log Data</strong></p> <p> I want to inform you that whenever you use my Service, in a case of
                    an error in the app I collect data and information (through third party products) on your phone
                    called Log Data. This Log Data may include information such as your device Internet Protocol (“IP”) address,
                    device name, operating system version, the configuration of the app when utilizing my Service,
                    the time and date of your use of the Service, and other statistics.
                  </p> <p><strong>Cookies</strong></p> <p>Cookies are files with a small amount of data that are commonly used as anonymous unique identifiers.
                    These are sent to your browser from the websites that you visit and are stored on your device's internal
                    memory.
                  </p> <p>This Service does not use these “cookies” explicitly. However, the app may use third party code and
                    libraries that use “cookies” to collect information and improve their services. You have the option to
                    either accept or refuse these cookies and know when a cookie is being sent to your device. If you choose
                    to refuse our cookies, you may not be able to use some portions of this Service.
                  </p> <p><strong>Service Providers</strong></p> <p> I may employ third-party companies and individuals due to the following reasons:</p> <ul><li>To facilitate our Service;</li> <li>To provide the Service on our behalf;</li> <li>To perform Service-related services; or</li> <li>To assist us in analyzing how our Service is used.</li></ul> <p> I want to inform users of this Service that these third parties have access to
                    your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However,
                    they are obligated not to disclose or use the information for any other purpose.
                  </p> <p><strong>Security</strong></p> <p> I value your trust in providing us your Personal Information, thus we are striving
                    to use commercially acceptable means of protecting it. But remember that no method of transmission over
                    the internet, or method of electronic storage is 100% secure and reliable, and I cannot guarantee
                    its absolute security.
                  </p> <p><strong>Links to Other Sites</strong></p> <p>This Service may contain links to other sites. If you click on a third-party link, you will be directed
                    to that site. Note that these external sites are not operated by me. Therefore, I strongly
                    advise you to review the Privacy Policy of these websites. I have no control over
                    and assume no responsibility for the content, privacy policies, or practices of any third-party sites
                    or services.
                  </p> <p><strong>Children’s Privacy</strong></p> <p>These Services do not address anyone under the age of 13. I do not knowingly collect
                    personally identifiable information from children under 13. In the case I discover that a child
                    under 13 has provided me with personal information, I immediately delete this from
                    our servers. If you are a parent or guardian and you are aware that your child has provided us with personal
                    information, please contact me so that I will be able to do necessary actions.
                  </p> <p><strong>Changes to This Privacy Policy</strong></p> <p> I may update our Privacy Policy from time to time. Thus, you are advised to review
                    this page periodically for any changes. I will notify you of any changes by posting
                    the new Privacy Policy on this page. These changes are effective immediately after they are posted on
                    this page.
                  </p> <p><strong>Contact Us</strong></p> <p>If you have any questions or suggestions about my Privacy Policy, do not hesitate to contact
                    me. Just say Hi! at hi [dog] sloth.pro
                  </p> <p>This privacy policy page was created at <a href="https://privacypolicytemplate.net" target="_blank">privacypolicytemplate.net</a>
                    and modified/generated by <a href="https://app-privacy-policy-generator.firebaseapp.com/" target="_blank">App
                      Privacy Policy Generator</a></p>
    </body>
    </html>
      
`
}
