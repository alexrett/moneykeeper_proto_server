FROM buildpack-deps:bionic

RUN apt-get update \
        && apt-get install -y --no-install-recommends unzip git golang-go sqlite3 go-dep \
    && rm -rf /var/lib/apt/lists/*

COPY . /root/go/src/app

WORKDIR /root/go/src/app
#RUN go get -u github.com/golang/dep/cmd/dep
#RUN dep ensure
#RUN dep status
RUN go build -o moneykeeper main.go